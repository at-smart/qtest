
Feature:
  Como Gerente, deseo quitar producto del menú para quitar productos poco pedidos o que den perdidas

  Scenario: Quitar un producto
    Given   que estoy en la lista de productos
    When    oprima el botón eliminar de un producto
    Then    se quita ese producto de la lista de productos del restaurante
