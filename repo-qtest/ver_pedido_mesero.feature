
Feature:
  Como Mesero, deseo ver un pedido de una mesa para poder enviarlo a cocina

  Scenario: Llega pedido
    Given   que llegue un pedido de una mesa
        And aparezca la notificación
    When    toque el número de la mesa
    Then    debo poder ver la lista de los productos y la mesa de dónde provino el pedido
    
  Scenario: Mesa con pedido
    Given   que hay una mesa con un pedido
    When    toque el botón de la mesa
    Then    se muestra la orden de la mesa
