
Feature:
  Como Mesero, deseo ver si un cliente me llama desde su mesa para poder ir a atenderlo

  Scenario: Cliente oprime el botón llamar mesero
    Given   que estoy en la página de atención de mesas
    When    un cliente oprima el botón llamar mesero
    Then    se mostrara una notificación de la mesa donde se hace el llamado
