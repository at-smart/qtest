
Feature:
  Como Cliente, deseo agregar uno o más productos a mi pedido

  Scenario: Agregar producto a la orden
    Given   que quiera agregar un producto
    When    oprima el botón “+” para agregar un plato (o más si se toca varias veces)
    Then    se agrega(n) el(los) plato(s) al pedido
    
  Scenario: Agregar más de un mismo plato
    Given   que ya agregue un plato
    When    vuelva a darle al botón “+”
    Then    este plato se agrega a la orden
    
  Scenario: Se hizo un pedido
    Given   que se haya hecho un pedido 
    And     aun no se haya pedido la cuenta
    When    de al botón “+” de un producto este se agrega al pedido
    Then    se pregunta si se desea hacer el pedido del producto agregado
    
  Scenario: Se pidió la cuenta
    Given   que se pidió la cuenta
    When    se vaya a agregar un producto a la orden
    Then    la orden no permite agregar más productos
