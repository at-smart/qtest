
Feature:
  Como Cliente, deseo quitar un producto del pedido para elegir un plato diferente si cambié de parecer y luego hacer el pedido

  Scenario: No se ha despachado el pedido
    Given   que estoy en la página del pedido
        And quiera quitar un plato de este 
        And este no haya sido ya despachado
    When    oprima el botón de quitar”-”
    Then    este plato se debe quitar del pedido

  Scenario: Se ha despachado el pedido
    Given   que un pedido ya ha sido pedido
        And el mesero lo haya marcado como despachado
    When    vaya a eliminar un producto de la orden desde el cliente de la mesa
    Then    se hace un llamado al mesero
    
  Scenario: Se pidió la cuenta
    Given   que se pidió la cuenta
    When    los productos ya hayan sido despachados
    Then    el cliente ya no puede eliminar el producto desde la terminal de cliente y se ocultan los botones de eliminar producto
