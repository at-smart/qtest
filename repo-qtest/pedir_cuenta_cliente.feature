
Feature:
  Como Cliente, deseo pedir la cuenta para pagar lo que he consumido

  Scenario: Para todos los escenarios
    Given   que este en la lista del pedido
    When    oprime el botón pedir cuenta
    Then    se muestra una notificación a los meseros que el cliente desea pagar la cuenta
