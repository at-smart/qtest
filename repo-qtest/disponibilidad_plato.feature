
Feature:
  Como Cliente, deseo ver disponibilidad de plato para ver que productos no puedo pedir

  Scenario: El plato no está disponible
    Given   hay un plato que no esté disponible 
        And se haya marcado como no disponible
    When    esté viendo el menú del restaurante
    Then    el plato no se debe mostrar
    
  Scenario: El plato está disponible
