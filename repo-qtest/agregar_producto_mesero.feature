
Feature:
  Como Mesero, deseo agregar uno o más productos al pedido de un cliente para cambiar el pedido de los clientes en caso de ellos requerir un cambio

  Scenario: No hay productos
    Given   que no hay productos en una orden
    When    oprima el botón”+” agregar producto a pedido
    Then    se agrega el producto al pedido y se actualiza el valor total del pedido
    
  Scenario: Hay productos en la orden
    Given   que hay productos en una orden
    When    dé al botón “+”
    Then    se agrega a la orden de mesa y se actualiza el valor total del pedido
