
Feature:
  Como Mesero, deseo ver disponibilidad de plato para ver que productos no puedo añadir al pedido de mi cliente

  Scenario: Para todos los escenarios
    Given   que esté en el menú de restaurante
    When    esté navegando los productos o vaya a agregarlo y no está disponible
    Then    se notifica que el producto no es válido para agregar al pedido