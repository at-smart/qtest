
Feature:
  Como Mesero, deseo ver el menú del restaurante para poder ver/buscar los productos que el cliente quiere adicionar a su pedido

  Scenario: No hay ordenes de mesa
    Given   que no hay ordenes de mesa
    When    dé al botón ver menú restaurante
    Then    se me presenta una ventana con el menú del restaurante, sin las opciones de agregar a orden
    
  Scenario: Hay orden de mesa
    Given   que hay una orden de mesa
        And dé ver orden de mesa
    When    dé al botón ver menú restaurante
    Then    se me presenta una ventana con el menú del restaurante, con la opción de agregar los platos a la orden de una mesa
